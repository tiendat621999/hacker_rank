import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

class Result {

    /*
     * Complete the 'countApplesAndOranges' function below.
     *
     * The function accepts following parameters:
     *  1. INTEGER s
     *  2. INTEGER t
     *  3. INTEGER a
     *  4. INTEGER b
     *  5. INTEGER_ARRAY apples
     *  6. INTEGER_ARRAY oranges
     */

    public static void countApplesAndOranges(int s, int t, int a, int b, List<Integer> apples, List<Integer> oranges) {
    // Write your code here

    }

}

public class Solution {
    public static void main(String[] args) {
        /* Khai báo biến tương ứng */
        Scanner scan = new Scanner(System.in);
        int s = scan.nextInt(); //điểm đầu mái nhà
        int t = scan.nextInt();  //điểm cuối mái nhà
        int a = scan.nextInt();    // lacate táo
        int b = scan.nextInt();     //locate cam
        int m = scan.nextInt();     // khoảng cách táo rơi từ trên cây
        int n = scan.nextInt();     // khoảng cách cam rơi từ trên cây
        
        /* Tính số táo rơi vào mái nhà */
        int applesOnHouse = 0;
        for (int i = 0; i < m; i++) {
            int applePosition = a + scan.nextInt();
            if (applePosition >= s && applePosition <= t) {
                applesOnHouse++;
            }
        }
        System.out.println(applesOnHouse);
        
        /* Tính số cam rơi vào mái nhà */
        int orangesOnHouse = 0;
        for (int i = 0; i < n; i++) {
            int orangePosition = b + scan.nextInt();
            if (orangePosition >= s && orangePosition <= t) {
                orangesOnHouse++;
            }
        }
        System.out.println(orangesOnHouse);
        
        scan.close();
    }
}
